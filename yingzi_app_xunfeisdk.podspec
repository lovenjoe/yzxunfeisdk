#
# Be sure to run `pod lib lint yingzi_app_xunfeisdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'yingzi_app_xunfeisdk'
  s.version          = '0.2.2'
  s.summary          = 'Xun fei SDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
是一个集成了讯飞SDK的插件。
                       DESC

  s.homepage         = 'https://gitee.com/lovenjoe/yzxunfeisdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yujingzhou' => 'yujingzhou@yingzi.com' }
  s.source           = { :git => 'https://gitee.com/lovenjoe/yzxunfeisdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.pod_target_xcconfig = {
    'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  s.user_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  
  s.ios.deployment_target = '13.0'

  s.source_files = 'yingzi_app_xunfeisdk/Classes/**/*'
  s.public_header_files = 'yingzi_app_xunfeisdk/Classes/**/*.h'
  s.resource = ['yingzi_app_xunfeisdk/Xunfei']
  
  #  第三方库
  s.vendored_frameworks = 'yingzi_app_xunfeisdk/Framework/iflyMSC.framework'
  
  # s.resource_bundles = {
  #   'yingzi_app_xunfeisdk' => ['yingzi_app_xunfeisdk/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'AVFoundation', 'SystemConfiguration', 'Foundation', 'CoreTelephony', 'AudioToolbox', 'UIKit', 'CoreLocation', 'QuartzCore', 'CoreGraphics', 'AddressBook', 'Contacts'
  s.libraries = 'c++', 'z'
  s.static_framework = true
  # s.dependency 'AFNetworking', '~> 2.3'
end
