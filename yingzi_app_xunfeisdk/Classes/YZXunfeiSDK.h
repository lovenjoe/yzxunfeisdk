//
//  YZXunfeiSDK.h
//  yingzi_app_xunfeisdk
//
//  Created by 余镜周 on 2024/4/30.
//

#import <Foundation/Foundation.h>
#import "YZXfSpeechSynthesizer.h"
#import "YZXfWakeupManager.h"
#import "YZXfSpeechManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface YZXunfeiSDK : NSObject

@end

NS_ASSUME_NONNULL_END
