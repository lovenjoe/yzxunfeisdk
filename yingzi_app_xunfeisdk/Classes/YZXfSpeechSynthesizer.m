//
//  YZXfSpeechSynthesizer.m
//  yingzi_flutter_hairender
//
//  Created by 余镜周 on 2024/4/22.
//

#import "YZXfSpeechSynthesizer.h"
#import "YZTTSConfig.h"

@interface YZXfSpeechSynthesizer()<IFlySpeechSynthesizerDelegate>

@property (nonatomic, strong) IFlySpeechSynthesizer *iFlySpeechSynthesizer;

@end

@implementation YZXfSpeechSynthesizer

+ (YZXfSpeechSynthesizer *)sharedManager {
    static YZXfSpeechSynthesizer *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [YZXfSpeechSynthesizer new];
    });
    return instance;
}

- (IFlySpeechSynthesizer *)iFlySpeechSynthesizer {
    if (!_iFlySpeechSynthesizer) {
        _iFlySpeechSynthesizer = [IFlySpeechSynthesizer sharedInstance];
        _iFlySpeechSynthesizer.delegate = self;
    }
    return _iFlySpeechSynthesizer;
}

- (void)start:(NSString *)text {
    //设置合成参数
    
    YZTTSConfig *instance = [YZTTSConfig sharedInstance];
    if (instance == nil) {
        return;
    }
    //set the resource path, only for offline TTS
    NSString *resPath = [[NSBundle mainBundle] resourcePath];
    NSString *newResPath = [[NSString alloc] initWithFormat:@"%@/Xunfei/resource/aisound/common.jet;%@/Xunfei/resource/aisound/xiaoyan.jet",resPath,resPath];
    [[IFlySpeechUtility getUtility] setParameter:@"tts" forKey:[IFlyResourceUtil ENGINE_START]];
    [self.iFlySpeechSynthesizer setParameter:newResPath forKey:@"tts_res_path"];

    //取消默认设置，解决会自动把唤醒停掉的问题（因为需要支持合成过程支持唤醒中断，合成结束后也要自动重启唤醒） http://bbs.xfyun.cn/thread/22826
    [self.iFlySpeechSynthesizer setParameter:@"0" forKey:[IFlySpeechConstant PLAYER_INIT]];
    [self.iFlySpeechSynthesizer setParameter:@"0" forKey:[IFlySpeechConstant PLAYER_DEACTIVE]];
    
    //set speed,range from 1 to 100.
    [self.iFlySpeechSynthesizer setParameter:instance.speed forKey:[IFlySpeechConstant SPEED]];
    
    //set volume,range from 1 to 100.
    [self.iFlySpeechSynthesizer setParameter:instance.volume forKey:[IFlySpeechConstant VOLUME]];
    
    //set pitch,range from 1 to 100.
    [self.iFlySpeechSynthesizer setParameter:instance.pitch forKey:[IFlySpeechConstant PITCH]];
    
    //set sample rate
    [self.iFlySpeechSynthesizer setParameter:instance.sampleRate forKey:[IFlySpeechConstant SAMPLE_RATE]];
    
    //set TTS speaker
    [self.iFlySpeechSynthesizer setParameter:instance.vcnName forKey:[IFlySpeechConstant VOICE_NAME]];
    
    //set text encoding mode
    [self.iFlySpeechSynthesizer setParameter:@"unicode" forKey:[IFlySpeechConstant TEXT_ENCODING]];
    
    //set engine type
    [self.iFlySpeechSynthesizer setParameter:instance.engineType forKey:[IFlySpeechConstant ENGINE_TYPE]];
    //启动合成会话
    [self.iFlySpeechSynthesizer startSpeaking: text];
    
}

- (void)stop {
    [self.iFlySpeechSynthesizer stopSpeaking];
}

#pragma mark - delegate

//合成结束
- (void) onCompleted:(IFlySpeechError *) error {
    if ([_delegate respondsToSelector:@selector(onCompleted:)]) {
        [_delegate onCompleted:error];
    }
}

//合成开始
- (void) onSpeakBegin {
    if ([_delegate respondsToSelector:@selector(onSpeakBegin)]) {
        [_delegate onSpeakBegin];
    }
}

//合成缓冲进度
/*
 广州今天天气多云转雷阵雨，气温17℃ - 29℃，体感温度24℃，湿度90%
 flutter: onBufferProgress: 35, ced=26
 flutter: onSpeakProgress: 0, 26
 flutter: onBufferProgress: 35, ced=26
 flutter: onBufferProgress: 66, ced=50
 flutter: onBufferProgress: 87, ced=66
 flutter: onBufferProgress: 100, ced=76
 flutter: onSpeakProgress: 0, 26
 flutter: onSpeakProgress: 26, 50
 flutter: onSpeakProgress: 50, 66
 flutter: onSpeakProgress: 66, 76
 */
- (void) onBufferProgress:(int) progress message:(NSString *)msg {
    if ([_delegate respondsToSelector:@selector(onBufferProgress:message:)]) {
        [_delegate onBufferProgress:progress message:msg];
    }
}

//合成播放进度
/*
 广州今天天气多云转雷阵雨，气温17℃ - 29℃，体感温度24℃，湿度90%
 分句输出：
 flutter: onSpeakProgress: 0, 26
 flutter: onSpeakProgress: 26, 50
 flutter: onSpeakProgress: 50, 66
 flutter: onSpeakProgress: 66, 76
 */
- (void) onSpeakProgress:(int) progress beginPos:(int)beginPos endPos:(int)endPos {
    if ([_delegate respondsToSelector:@selector(onSpeakProgress:beginPos:endPos:)]) {
        [_delegate onSpeakProgress:progress beginPos:beginPos endPos:endPos];
    }
}

@end
