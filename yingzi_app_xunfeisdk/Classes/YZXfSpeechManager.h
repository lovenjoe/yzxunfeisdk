//
//  SpeechManager.h
//  SDKSample
//
//  Created by niren on 2023/2/15.
//

#import <Foundation/Foundation.h>
#import "iflyMSC/iflyMSC.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^_Nullable YZSpeechCompletion)(NSString *text);
typedef void(^_Nullable YZSpeechCancel)(void);

@protocol YZXfSpeechManagerDelegate <NSObject>

@optional
/*!
 * 录音开始
 */
- (void)onBeginOfSpeech;
/*!
 * 录音结束
 */
- (void)onEndOfSpeech;
- (void)onCancel;
- (void)onCompleted:(IFlySpeechError *)error;
- (void)onResults:(NSArray *)results isLast:(BOOL)isLast parseText:(NSString *)parseText;


@end

@interface YZXfSpeechManager : NSObject

@property (nonatomic, assign) id<YZXfSpeechManagerDelegate> delegate;
@property (nonatomic, assign, readonly) BOOL isListening;


+ (YZXfSpeechManager *)sharedManager;

- (void)initSDKWithAppid:(NSString *)appid;

- (void)setDefaultRecognizerMode;

/// 开始一路会话
- (void)startListening;

/// 停止本次会话，将会有回调
- (void)stopListening:(YZSpeechCompletion)completion;

/// 取消本次会话
- (void)cancel:(YZSpeechCancel)completion;

@end

NS_ASSUME_NONNULL_END
