//
//  YZXfSpeechSynthesizer.h
//  yingzi_flutter_hairender
//
//  Created by 余镜周 on 2024/4/22.
//

#import <Foundation/Foundation.h>
#import <iflyMSC/iflyMSC.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YZXfSpeechSynthesizerDelegate <NSObject>

@optional
//合成结束
- (void) onCompleted:(IFlySpeechError *) error;
//合成开始
- (void) onSpeakBegin;
//合成缓冲进度
- (void) onBufferProgress:(int) progress message:(NSString *)msg;
//合成播放进度
- (void) onSpeakProgress:(int) progress beginPos:(int)beginPos endPos:(int)endPos;

@end

@interface YZXfSpeechSynthesizer : NSObject

+ (YZXfSpeechSynthesizer *)sharedManager;

@property (nonatomic, assign) id<YZXfSpeechSynthesizerDelegate> delegate;
@property (nonatomic, assign, readonly) BOOL isListening;

- (void)start:(NSString *)text;
- (void)stop;

@end

NS_ASSUME_NONNULL_END
