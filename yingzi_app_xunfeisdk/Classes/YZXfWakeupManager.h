//
//  WakeupManager.h
//  yingzi_flutter_hairender
//
//  Created by 余镜周 on 2024/4/18.
//

#import <Foundation/Foundation.h>
#import <iflyMSC/IFlyMSC.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YZXfWakeupManagerDelegate <NSObject>

@optional
/*!
 * 录音开始
 */
- (void)onBeginOfSpeech;
/*!
 * 录音结束
 */
- (void)onEndOfSpeech;
/*!
 * 会话错误
 *
 * @param error 错误描述类，
 */
- (void)onCompleted:(IFlySpeechError *)error;
/*!
 * 唤醒结果
 *
 * @param resultDic 唤醒结果字典
 */
- (void)onResult:(NSMutableDictionary *)resultDic;
/*!
 * 音量反馈，返回频率与录音数据返回回调频率一致
 *
 * @param volume 音量值
 */
- (void)onVolumeChanged:(int)volume;

@end

@interface YZXfWakeupManager : NSObject

+ (YZXfWakeupManager *)sharedManager;

@property (nonatomic, assign) id<YZXfWakeupManagerDelegate> delegate;
@property (nonatomic, assign, readonly) BOOL isListening;

- (BOOL)start;
- (BOOL)stop;

@end

NS_ASSUME_NONNULL_END
