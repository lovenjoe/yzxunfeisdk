#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "YZXfSpeechManager.h"
#import "YZXfSpeechSynthesizer.h"
#import "YZTTSConfig.h"
#import "YZXfWakeupManager.h"
#import "YZXunfeiSDK.h"

FOUNDATION_EXPORT double yingzi_app_xunfeisdkVersionNumber;
FOUNDATION_EXPORT const unsigned char yingzi_app_xunfeisdkVersionString[];

