//
//  main.m
//  yingzi_app_xunfeisdk
//
//  Created by 余镜周 on 04/30/2024.
//  Copyright (c) 2024 余镜周. All rights reserved.
//

@import UIKit;
#import "YZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YZAppDelegate class]));
    }
}
