//
//  YZAppDelegate.h
//  yingzi_app_xunfeisdk
//
//  Created by 余镜周 on 04/30/2024.
//  Copyright (c) 2024 余镜周. All rights reserved.
//

@import UIKit;

@interface YZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
